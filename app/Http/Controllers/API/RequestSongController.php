<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\RequestSongModel;
use Illuminate\Http\Request;
use function response;
use const true;

class RequestSongController extends Controller
{

    public function index()
    {
        $requestSong = RequestSongModel::all();
        return response()->json(['DATA' => $requestSong], 200);

    }

    public function store(Request $request)
    {
        $rule = [
            'song_name' => 'required',
            'singer_name' => 'required',

        ];

        $this->validate($request, $rule);
        $requestSong = RequestSongModel::create($request->all());

        return response()->json([
            'STATUS' => true,
            'DATA' => $requestSong],
            201);
    }

    public function show($id)
    {

    }

}
