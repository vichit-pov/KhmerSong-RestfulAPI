<?php

namespace App\Http\Controllers\CMS;

use App\SingerModel;
use function compact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function redirect;
use function view;

class SingerControllerCMS extends Controller
{

    public function index()
    {

//        $singers = SingerModel::with('song')->get();
        $singers = SingerModel::orderBy('id','desc')->paginate('8');
        return view('dashboard.singers', compact('singers'));


    }

    public function create()
    {

        return view('dashboard.create_singer');
    }

    public function store(Request $request)
    {
        $singer = new SingerModel();

        $this->validate($request, [
            'txtSingerName' => 'required',
            'txtSingerImage' => 'required',
        ]);

        $singer->name = $request->txtSingerName;
        $singer->image = $request->txtSingerImage;

        $request->session()->flash('message', '<div class="alert alert-success">Congratulations Insert</div>');
        $singer->save();
        return back();
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $singers = SingerModel::get()->find($id);
        return view('detail.update_singer',compact('singers'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'txtSingerName' => 'required|min:5',
            'txtSingerImage' => 'required|min:10',
        ]);
        $singer = SingerModel::find($id);
        $singer->name = $request->txtSingerName;
        $singer->image = $request->txtSingerImage;
        $singer->save();
        $request->session()->flash('message', '<div class="alert alert-success">Updated Successfully</div>');
        return redirect('singers/');

    }

    public function destroy($id)
    {
        $singer = SingerModel::find($id);
        $singer->delete();
        session()->flash('message', '<div class="alert alert-success">Delete Successfully</div>');
        return redirect('singers/');

    }
}
