<?php

namespace App\Http\Controllers\CMS;

use App\CategoryModel;
use App\SingerModel;
use App\SongModel;
use function back;
use function compact;
use function dd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use function redirect;
use function view;

class SongControllerCMS extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function songs(){
        //$songs = SongModel::get()->load('category', 'singer', 'type');
        //return view('dashboard.songs', compact('songs'));

        $songs = SongModel::where('types_id', 2)
            ->orderBy('id', 'desc')
            ->paginate(8);

        return view('dashboard.songs', compact('songs'));
    }

    //show create_song view
    public function create()
    {
        $allSinger = SingerModel::with('song')->get();
        $allCategory = CategoryModel::with('song')->get();
        return view('dashboard.create_song', compact('allSinger', 'allCategory'));
    }

    //insert
    public function store(Request $request)
    {
        $song = new SongModel();

        $this->validate($request, [
            'txtSongName' => 'required',
            'txtSongURL' => 'required',
        ]);

        $song->name = $request->txtSongName;
        $song->url = $request->txtSongURL;
        $song->singers_id = $request->cbSingerName;
        $song->categories_id = $request->cbCategory;
        $song->types_id = "2";

        $request->session()->flash('message', '<div class="alert alert-success">Congratulations Insert</div>');
        $song->save();
        return back();
    }

    //show by id
    public function edit($id)
    {
        $songs = SongModel::get()->load('category', 'singer', 'type')->find($id);
        $allSinger = SingerModel::with('song')->get();

        return view('detail.update', compact('songs', 'allSinger'));

    }


    //update song
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'txtSongName' => 'required|min:5',
            'txtSongURL' => 'required|min:10',
        ]);


//        $id = $request->txtID;
//        SongModel::where('id', $id)->update([
//            'name' => $request->txtSongName,
//            'url' => $request->txtSongURL,
//            'categories_id' => $request->cbCategory,
//            'singers_id' => $request->cbSingerName,
//        ]);

        $song = SongModel::find($id);
        $song->name = $request->txtSongName;
        $song->url = $request->txtSongURL;
        $song->categories_id = $request->cbCategory;
        $song->singers_id = $request->cbSingerName;

        $song->save();
        $request->session()->flash('message', '<div class="alert alert-success">Updated Successfully</div>');
        return redirect('songs/');

    }

    public function destroy($id)
    {

        $song = SongModel::find($id);
        $song->delete();
        session()->flash('message', '<div class="alert alert-danger">Delete Successfully</div>');
        return redirect('/songs');
    }


}
