<?php

namespace App;

use App\SongModel;
use Illuminate\Database\Eloquent\Model;

class SingerModel extends Model
{
    protected $table = 'singers';
    protected $hidden =[
        'created_at',
        'updated_at',
    ];

    public function song()
    {
        return $this->hasMany(SongModel::class,'singers_id');
    }
}
