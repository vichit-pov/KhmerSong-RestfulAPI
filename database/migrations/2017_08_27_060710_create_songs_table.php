<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id',10);
            $table->string('name',100)->nullalbe();
            $table->string('url',500)->nullalbe();
            $table->integer('categories_id')->unsigned();
            $table->integer('singers_id')->unsigned();
            $table->integer('types_id')->unsigned();
            $table->timestamps();

            $table->foreign('categories_id')->references('id')->on('categories');
            $table->foreign('singers_id')->references('id')->on('singers');
            $table->foreign('types_id')->references('id')->on('types');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
