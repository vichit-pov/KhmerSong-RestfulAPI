@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">SINGERS</h1>
    <p class="text-center">{!! session('message') !!}</p>
    <table class="table table-striped table-inverse">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Image URL</th>
            <th>Action</th>
            <th><a href="{{'/singers/create'}}">
                    <button class="btn btn-default">Add New</button>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($singers as $s)
            <tr>
                <th scope="row">{{ $s->id }}</th>
                <td>{{ $s->name }}</td>
                <td>{{ str_limit($s->image, $limit = 30) }}</td>
                <td>
                    <button type="button" , class="btn btn-default">
                        <a class="text-primary" href="/singers/edit/{{$s->id}}">Update</a>
                    </button>
                </td>
                <td>
                    <form method="POST" action="{{'/singers/'.$s->id}}">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-center">
        {!! $singers->render() !!}
    </div>
@stop
