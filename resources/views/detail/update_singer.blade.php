@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">Update Singer</h1>
    <p class="text-center">{!! session('message') !!}</p>

    <Form method="POST" action="{{url('singers/'.$singers->id)}}">
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group">
            <input type="text" class="form-control" id="txtSingerName" name="txtSingerName" value="{{$singers->name}}">
            <p class="text-danger">{{$errors->first('txtSingerName')}}</p>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="txtSingerImage" name="txtSingerImage"
                   value="{{$singers->image}}">
            <p class="text-danger">{{$errors->first('txtSingerImage')}}</p>
        </div>
        <div class="form-group-lg">
            <button class="btn btn-danger" type="submit">Update</button>
        </div>

    </Form>
@endsection