@extends('layouts.app')
@section('content')

        <!DOCTYPE html>
<html>
<head>

    <title>@yield('title')</title>

</head>
<body>

@section('menu-left')
    <div class="col-md-2">
        <div class="list-group">
            <a class="list-group-item active">Dashboard</a>
            <a class="list-group-item" href="{{'/songs'}}">All Song</a>
            <a class="list-group-item" href="{{'/songs/create'}}">Create Songs</a>
            <a class="list-group-item" href="{{'/category'}}">Category</a>
            <a class="list-group-item" href="{{'/singers'}}">Singer</a>
            <a class="list-group-item" href="{{'/signup'}}">Register</a>
        </div>
    </div>
@show

<div class="col-md-10">
    @yield('content-song')
</div>

</body>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</html>
@endsection