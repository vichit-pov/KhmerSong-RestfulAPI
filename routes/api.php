<?php

use Illuminate\Http\Request;


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


//Song
Route::resource('/api/v1/songs', 'API\SongController', ['only' => ['index', 'show',]]);

//Category
Route::resource('api/v1/categories', 'API\CategoryController', ['only' => ['index', 'show',]]);
Route::resource('api/v1/categories.songs', 'API\CategorySongController', ['only' => ['index']]);

//Singer
Route::resource('/api/v1/singers', 'API\SingerController', ['only' => ['index', 'show',]]);
Route::resource('/api/v1/singers.songs', 'API\SingerSongController', ['only' => ['index']]);

//Type
Route::resource('/api/v1/types', 'API\TypeController', ['only' => ['index', 'show',]]);
Route::resource('/api/v1/types.songs', 'API\TypeSongController', ['only' => ['index']]);


//RequestSong from user
Route::resource('/api/v1/request', 'API\RequestSongController', ['only' => ['index', 'show', 'store']]);











