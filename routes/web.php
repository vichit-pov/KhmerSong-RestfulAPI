<?php

//Songs
Route::get('/', 'CMS\SongControllerCMS@index');
Auth::routes();

Route::middleware('auth')->group(function (){
    Route::get('songs/', 'CMS\SongControllerCMS@songs');
    Route::get('songs/create/', 'CMS\SongControllerCMS@create');
    Route::post('songs/', 'CMS\SongControllerCMS@store');
    Route::get('songs/edit/{id}/', 'CMS\SongControllerCMS@edit');
    Route::put('songs/{id}/', 'CMS\SongControllerCMS@update');
    Route::delete('songs/{id}/', 'CMS\SongControllerCMS@destroy');

    //Category
    Route::get('category/', 'CMS\CategoryControllerCMS@index');
    Route::get('category/create', 'CMS\CategoryControllerCMS@create');
    Route::post('category/', 'CMS\CategoryControllerCMS@store');
    Route::get('category/edit/{id}/', 'CMS\CategoryControllerCMS@edit');
    Route::put('category/{id}/', 'CMS\CategoryControllerCMS@update');
    Route::delete('category/{id}/', 'CMS\CategoryControllerCMS@destroy');


    //Singers
    Route::get('singers/', 'CMS\SingerControllerCMS@index');
    Route::get('singers/create/', 'CMS\SingerControllerCMS@create');
    Route::post('singers/', 'CMS\SingerControllerCMS@store');
    Route::get('singers/edit/{id}/', 'CMS\SingerControllerCMS@edit');
    Route::put('singers/{id}/', 'CMS\SingerControllerCMS@update');
    Route::delete('singers/{id}/', 'CMS\SingerControllerCMS@destroy');


    Route::get('/home', 'CMS\HomeController@index')->name('home');
    Route::get('/signup', 'CMS\RegisterAdminController@index');
    Route::post('/signup', 'CMS\RegisterAdminController@store');

    Route::resource('/test', 'TestController');
});






